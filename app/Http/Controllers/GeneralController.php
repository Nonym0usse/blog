<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function index()
    {
      return view('index');
    }

    public function entreprise()
    {
        return view('entreprise.entreprise');
    }

    public function activites()
    {
      return view('activites.activites');
    }

    public function bonus()
    {
        return view('bonus.bonus');
    }

    public function activites1()
    {
        return view('activites.un');
    }

    public function activites2()
    {
        return view('activites.deux');
    }
    public function activites3()
    {
        return view('activites.trois');
    }

    public function activites4()
    {
        return view('activites.quatre');
    }

    public function activites5()
    {
        return view('activites.cinq');
    }

    public function conclusion()
    {
        return view('activites.conclusion');
    }
}
