<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['as' => '/', 'uses' => 'GeneralController@index']);
Route::get('/entreprise', ['as' => 'entreprise', 'uses' => 'GeneralController@entreprise']);
Route::get('/activites', ['as' => 'activites', 'uses' => 'GeneralController@activites']);
Route::get('/bonus', ['as' => 'bonus', 'uses' => 'GeneralController@bonus']);

Route::get('activites/1', ['as' => 'activites.1', 'uses' => 'GeneralController@activites1']);
Route::get('activites/2', ['as' => 'activites.2', 'uses' => 'GeneralController@activites2']);
Route::get('activites/3', ['as' => 'activites.3', 'uses' => 'GeneralController@activites3']);
Route::get('activites/4', ['as' => 'activites.4', 'uses' => 'GeneralController@activites4']);
Route::get('activites/5', ['as' => 'activites.5', 'uses' => 'GeneralController@activites5']);
Route::get('activites/conclusion', ['as' => 'activites.conclusion', 'uses' => 'GeneralController@conclusion']);
