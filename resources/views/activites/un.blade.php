@extends('app')
@section('content')

    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i> MonBlog</a>
            </div>
            <div class="navbar-collapse collapse" id="bs-navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="{{ route('/') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('entreprise') }}">L'entreprise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('activites') }}">Mes activités</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll"  href="#aboutModal">CYRIL VELLA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="banner">
        <div class="banner-text">
            <b><h1 style="background-color: #000; opacity: 0.5;">BLOG DE STAGE - CYRIL VELLA</h1></b>
        </div>
    </div>
    <h1 class="cursive" style="text-align: center; padding-top: 4%;">Période du 29 mai au 14 Juin 2017</h1>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 blogShort">
                    <img src="css/vagrant.png" alt="post img" class="pull-left img-responsive postImg img-thumbnail margin10">
                    <article><p>
                            Cela fait maintenant deux semaines que je travaille chez Immo-one en tant que développeur web. Les horaires sont assez souples, puisque nous pouvons arriver et partir à l’heure que nous décidons.
                            Cependant, la seule chose qui est demandé est que nous ayons terminé le travail à temps, ce qui nous oblige à avoir un peu de rigueur et donc à arriver bien souvent au maximum à 9h et partir à 17h30.
                            Le premier jour, j’ai pu avoir mon propre poste de travail. J’ai dû installer mon environnement de travail à savoir une machine virtuelle Linux sous Vagrant avec le logiciel Nginx sans avoir aucune notion du fonctionnement.
                            Je me suis donc documenté sur le web et j’ai demandé de l’aide à mes collaborateurs.

                        </p>
                        <p>
                            Les jours suivants, mon maître de stage et aussi mon chef de projet, Mathieu Chevalier, m’a donc conseillé de me former sur le Framework Laravel grâce au site Laracasts.
                            Ce sont des vidéos en anglais uniquement qui durent en vingtaines de minutes par vidéo. Je me suis formé pendant au moins une quinzaine de jours.
                            Pendant ce temps, je me suis entraîné à faire ce blog avec le Framework. J’ai aussi demandé à être formé sur le métier d’agent immobilier afin de comprendre mieux le secteur d’activité.
                        </p>
                        <p>
                            Pendant ma formation, j’ai quand même voulu participé aux projets de développement du site et j’ai ainsi pu commencer à développer ma propre fonctionnalité en fonction d’un cahier des charges.
                            Il s’agissait de réaliser une calculatrice financière pour les agents immobiliers. J’ai élaboré un design simple avec les couleurs du logiciel et il fallait rendre la calculatrice accessible de partout dans le projet.
                            J’ai donc pensé qui fallait créer une modal avec Bootstrap comme ça, pas besoin de naviguer dans le site quand on veut faire un simple calcul. J’ai quand même rencontré quelques difficultés…
                            La première était de s’adapter aux très nombreux fichiers présents dans le projet et aux fonctions déjà codées auparavant pour ne pas faire des répétitions de codes inutiles.
                            Deuxièmement, j’avais placé mon CSS dans un nouveau dossier alors qu’il fallait juste mettre mon CSS dans des fichiers déjà existant,
                            mais la seule différence et qu’il fallait compiler son CSS… Oui, le CSS se compile avec la ligne de commande « Gulp » à l’aide de l’invité de commande. Ce qui donne un fichier LESS que Laravel peut interpréter.
                        </p>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <b><i><h3 style="text-align: center;">Voici la plateforme sur laquelle je me suis formé - Laracasts</h3></i></b>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-2">
                    <img src="css/laracast.png" class="e2" alt="Laracact_photo">
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row contact">
                <div class="col-md-6 text-right">
                    <div class="contacts-data">
                        <h3 >A propos de moi</h3>
                        <a href="#" class="btn btn-default transparent">Mon CV <i class="fa fa-arrow-right button-icon"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contacts-data">
                        <i class="fa fa-paper-plane fa-2x"></i>
                        <span class="contact-text">cyril.vella@yahoo.com</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-phone fa-2x"></i>
                        <span class="contact-text">06.52.41.51.45</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-skype fa-2x"></i>
                        <span class="contact-text">cyril.vella</span>
                    </div>
                </div>
            </div>
            <p class="text-center">
                TOUS DROITS RESERVES. 2017
            </p>
        </div>
    </footer>
