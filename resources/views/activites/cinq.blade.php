@extends('app')
@section('content')
    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i> MonBlog</a>
            </div>
            <div class="navbar-collapse collapse" id="bs-navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="{{ route('/') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('entreprise') }}">L'entreprise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('activites') }}">Mes activités</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll"  href="#aboutModal">CYRIL VELLA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="banner">
        <div class="banner-text">
            <b><h1 style="background-color: #000; opacity: 0.5;">BLOG DE STAGE - CYRIL VELLA</h1></b>
        </div>
    </div>
    <h1 class="cursive" style="text-align: center; padding-top: 4%;">Période du 29 juillet au 14 août 2017</h1>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 blogShort">
                    <img src="/css/pdf.png" alt="post img" class="pull-left img-responsive postImg img-thumbnail margin10">
                    <article>
                        <p>
                            Pendant ces deux semaines, j’ai passé les trois premiers jours à faire du nettoyage dans la société avec l’accord de M. Levy afin de libérer les locaux pour septembre et ainsi prévoir la destruction des murs pour avoir bureaux en open-space.
                        </p>
                        <p>
                            Ensuite, étant donné que nous étions en effectif réduit (vacances), il n’y avait pas de service commercial, j’ai dû donc prendre les appels et les mails le matin uniquement pour renseigner les clients. En termes de développement, j’ai continué à développer des petits modules pour le projet comme par exemple : générer une fiche PDF à partir d’éléments des informations du site. Fiche PDF, que j’ai précédemment designer avec Photoshop puis que j’ai implémenter avec du HTML et du CSS. J’ai énormément appris en PHP, notamment la programmation orientée objet qui n’était pas mon point fort à l’école, j’arrive donc à traiter certains jours jusqu’à cinq tickets mantis par jour.
                            Autre demande de la part du service commercial, il a été noté que beaucoup de clients avaient leurs photos à l'envers dès qu'ils envoyaient leurs photos depuis un smartphone.
                            J'ai développé un outil de rotation automatique des photos (grâce à une librairie PHP externe) et j'ai élaboré un outil de rognement manuelle (librairie JCrop en JS), ces outils sont disponibles après l'upload des photos sur le serveur.
                        <p>
                            Mais il me reste une vingtaine de tickets à traiter avant la fin de mon stage dont certains que je mets beaucoup plus de temps en raison de leur complexité à réaliser ou en général ils sont
                            faciles mais longs à traiter.Durant les derniers jours je me suis occupé du parc informatique de la société. En effet, nous sommes passés à la fibre et le changement d’opérateur à provoquer la réinitialisation de l’ensemble des équipements réseaux dont certains de répondaient plus. Ma mission était de reconnecter les machines sur les bonnes plages d’adresses IP dont la base de données locale, les imprimantes et les postes ainsi que les serveurs NAS. Il fallait donc comprendre le fonctionnement l’ensemble du réseau informatique qui fut vaste. Pour finir, deux postes informatiques sont tombés en pannes durant le mois.
                            Dans le stock de la société, nous avons beaucoup de composants / pièces détachées, j’ai alors détecté la panne du matériel défectueux et j’ai donc pu changer les pièces par d’autres.
                            J’ai réussi à réparer les deux postes en moins d’une après-midi.
                        </p>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <b><i><h3 style="text-align: center;">Développement de l'outil de rotation de photos après l'upload</h3></i></b>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-1">
                    <img src="/css/rotation_automatique.png" class="e3" alt="Postegree SQL">
                </div>
            </div>
        </div>
    </section>

    <b><i><h3 style="text-align: center;">Génération des fiches des biens en PDF</h3></i></b>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-1">
                    <img src="/css/imprimer_fiche.png" class="e3" alt="Postegree SQL">
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row contact">
                <div class="col-md-6 text-right">
                    <div class="contacts-data">
                        <h3 >A propos de moi</h3>
                        <a href="#" class="btn btn-default transparent">Mon CV <i class="fa fa-arrow-right button-icon"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contacts-data">
                        <i class="fa fa-paper-plane fa-2x"></i>
                        <span class="contact-text">cyril.vella@yahoo.com</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-phone fa-2x"></i>
                        <span class="contact-text">06.52.41.51.45</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-skype fa-2x"></i>
                        <span class="contact-text">cyril.vella</span>
                    </div>
                </div>
            </div>
            <p class="text-center">
                TOUS DROITS RESERVES. 2017
            </p>
        </div>
    </footer>
