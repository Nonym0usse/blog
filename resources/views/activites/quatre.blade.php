@extends('app')
@section('content')
    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i> MonBlog</a>
            </div>
            <div class="navbar-collapse collapse" id="bs-navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="{{ route('/') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('entreprise') }}">L'entreprise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('activites') }}">Mes activités</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll"  href="#aboutModal">CYRIL VELLA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="banner">
        <div class="banner-text">
            <b><h1 style="background-color: #000; opacity: 0.5;">BLOG DE STAGE - CYRIL VELLA</h1></b>
        </div>
    </div>
    <h1 class="cursive" style="text-align: center; padding-top: 4%;">Période du 14 juillet au 29 juillet 2017</h1>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 blogShort">
                    <img src="/css/xml.png" alt="post img" class="pull-left img-responsive postImg img-thumbnail margin10">
                    <article><p>
                            Ces deux semaines ont été consacrés à la même est unique tâche. Il fallait faire un formulaire contenant plus d’une quarantaine de champs à remplir par l’agent immobilier pour une agence spécifique, en respectant un mini cahier des charges qui a été établi, il y a plus d’une dizaine d’années sur un logiciel ayant son propre langage spécifique (webDev 18).
                            Il s’agissait de réaliser une page pour renseigner une estimation sur un bien.
                            J’ai dû alors transformer et améliorer cet algorithme avec PHP. Ce « cahier des charges » était établi de la sorte : « Chaque champs à renseigner HTML (boutons radios, boutons checkbox, etc…), retourne un entier en puissance de deux. Si l’utilisateur coche un champ alors on a une variable qui s’incrémente et le résultat final une fois les champs cochés,
                            seront inscrits dans un fichier XML, qui sera automatiquement envoyé sur le serveur de l’agence » (évidemment aucune notice a été fourni de la part de cette agence concernant, ce qui rendait le travail assez corsé).

                        </p>
                        <p>
                            Evidemment, une estimation prend en compte plusieurs types de biens que ce soit un appartement à vendre ou une maison. Selon de type de bien à vendre, il n’y a pas le même nombre de champs à remplir, il fallait donc absolument faire attention aux valeurs que l’on renvoyait pour ne pas fausser le traitement en XML.                        </p>
                        <p>
                            Ce module fut un des plus difficiles à coder. Pourquoi ? Il fallait tout d’abord décider ou mettre cette page unique dans le projet sans impacter les autres tables dans la base de données en créant une table spécifique contenant plus d’une quarantaine de colonnes.
                            Deuxièmement, il faut que cette page soit visible uniquement par l’agence en question et non pas par les autres agences).
                            Au final, j’ai réussi à faire fonctionner en très grande partie ce module, mais il reste encore quelques points à améliorer notamment lors de la génération du XML,
                            j’ai complètement oublié de le stocker en base de données. En soit, ce n’est pas important, mais en cas de litige avec le client, par exemple s’il manque une valeur dans le XML,
                            nous sommes capables de ressortir le tout de la base de données et afficher nos valeurs dans un navigateur quelconque pour montrer que nous faisons le traitement des estimations correctes et ainsi se décharge de toute responsabilité.
                        </p>
                    </article>
                </div>
            </div>
        </div>
    </section>
    <b><i><h3 style="text-align: center;">Voici la page des estimations pour l'agence Multiexpertise ERA France</h3></i></b>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-1">
                    <img src="/css/estimation_era.png" class="e3" alt="Postegree SQL">
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row contact">
                <div class="col-md-6 text-right">
                    <div class="contacts-data">
                        <h3 >A propos de moi</h3>
                        <a href="#" class="btn btn-default transparent">Mon CV <i class="fa fa-arrow-right button-icon"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contacts-data">
                        <i class="fa fa-paper-plane fa-2x"></i>
                        <span class="contact-text">cyril.vella@yahoo.com</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-phone fa-2x"></i>
                        <span class="contact-text">06.52.41.51.45</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-skype fa-2x"></i>
                        <span class="contact-text">cyril.vella</span>
                    </div>
                </div>
            </div>
            <p class="text-center">
                TOUS DROITS RESERVES. 2017
            </p>
        </div>
    </footer>
