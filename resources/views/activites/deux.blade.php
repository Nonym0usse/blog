@extends('app')
@section('content')
    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i> MonBlog</a>
            </div>
            <div class="navbar-collapse collapse" id="bs-navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="{{ route('/') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('entreprise') }}">L'entreprise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('activites') }}">Mes activités</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll"  href="#aboutModal">CYRIL VELLA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="banner">
        <div class="banner-text">
            <b><h1 style="background-color: #000; opacity: 0.5;">BLOG DE STAGE - CYRIL VELLA</h1></b>
        </div>
    </div>
    <h1 class="cursive" style="text-align: center; padding-top: 4%;">Période du 14 juin au 29 juin 2017</h1>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 blogShort" style="padding-bottom: 8%;">
                    <article><p>
                            Cela fait maintenant plus de deux semaines que je suis un membre de l’équipage. J’en sais maintenant un peu plus chaque jour sur mon environnement de travail.
                            Je sais que la société possède plusieurs serveurs sous Windows Server 2008, avec trois bases de données, une base pour la production (hébergement du site, avec les données confidentielles de chaque utilisateur), une base de test qui est une réplique exacte de la première base pour faire nos tests,
                            (j’ai d’ailleurs supprimé une table par inadvertance sur la base de test faisant planter partiellement le site en local, ouf !).  Chaque développeur de l’équipe est relié à la base de test.
                            Ensuite, j’ai appris à manipuler l’outil Git qui permet de fusionner son propre code avec celui des autres. J’avais un peu du mal au début parce que l’outil est un peu complexe à comprendre. Et si on fait une mauvaise manipulation, on peut très facilement perdre notre travail que nous avons fait ou encore rendre la mise en production difficile si on push sur la mauvaise branche
                            (il faut absolument créer sa propre branche à partir de master, puis on push sur sa propre branche, pour que M. Basquin puisse récupérer mon travail en local pour être mis en production).
                        </p>
                        <p>
                            Chaque membre possède un compte Outlook pour les mails et un compte sur le logiciel Mantis. Ce programme, est un site web hébergé en local sur nos serveurs, nous permet de voir des « tickets », un ticket correspond à une tâche à traiter dans l’immédiat, l’urgence, ou plus tard.
                            En général, il s’agit de bugs à résoudre et de fonctionnalités à implémenter dans le site, les tickets sont créés par M. Labourine du service de Hotline qui prend en compte les avis des clients.
                            Voici un exemple de mes créations que j’ai pu développer (parfois à l’aide d’autres développeurs pour m’initier aux consignes et à m’adapter et fonctions du code qui fut déjà développer auparavant par quelqu’un d’autre.

                        </p>
                        <p>
                            Ma deuxième mission était de développer un système permettant d’envoyer un message aux directeurs d’agences et aux agents immobiliers pour les réseaux de mandataires.
                            J’ai réussi à faire environ 70% de ma mission et le reste je fus accompagné par M. Basquin à cause de l’implémentation de mes fonctions dans certains fichiers (droits de lectures, récupérer la variable de session, créer un cookie avec une date à l’intérieur).
                            Je devais faire apparaître ce message une seule fois dès que l’utilisateur a vu le message.
                            Evidemment, d’autres tâches ont été effectuées en parallèle pour ma part durant les deux semaines …
                        </p>
                    </article>
                    <img src="/css/message.png" alt="post img" class="pull-left img-responsive postImg img-thumbnail margin10">
                </div>
            </div>
        </div>
    </section>
    <b><i><h3 style="text-align: center;">Notre logiciel de gestion de tâches / bugs - Mantis</h3></i></b>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-1">
                    <img src="/css/mantis.png" class="e3" alt="Laracact_photo">
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row contact">
                <div class="col-md-6 text-right">
                    <div class="contacts-data">
                        <h3 >A propos de moi</h3>
                        <a href="#" class="btn btn-default transparent">Mon CV <i class="fa fa-arrow-right button-icon"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contacts-data">
                        <i class="fa fa-paper-plane fa-2x"></i>
                        <span class="contact-text">cyril.vella@yahoo.com</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-phone fa-2x"></i>
                        <span class="contact-text">06.52.41.51.45</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-skype fa-2x"></i>
                        <span class="contact-text">cyril.vella</span>
                    </div>
                </div>
            </div>
            <p class="text-center">
                TOUS DROITS RESERVES. 2017
            </p>
        </div>
    </footer>
