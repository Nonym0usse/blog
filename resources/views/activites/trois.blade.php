@extends('app')
@section('content')
    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i> MonBlog</a>
            </div>
            <div class="navbar-collapse collapse" id="bs-navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="{{ route('/') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('entreprise') }}">L'entreprise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('activites') }}">Mes activités</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll"  href="#aboutModal">CYRIL VELLA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="banner">
        <div class="banner-text">
            <b><h1 style="background-color: #000; opacity: 0.5;">BLOG DE STAGE - CYRIL VELLA</h1></b>
        </div>
    </div>
    <h1 class="cursive" style="text-align: center; padding-top: 4%;">Période du 29 juin au 14 juillet 2017</h1>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 blogShort">
                    <img src="/css/sql_server.png" alt="post img" class="pull-left img-responsive postImg img-thumbnail margin10">
                    <article><p>
                            Cela fait 1 mois que j’ai intégré l’équipe, mais malgré cela j’ai parfois un peu de mal à comprendre le métier d’agent immobilier
                            (loi ALUR, quelques termes techniques parfois un peu complexe à assimiler…). Pendant ces quinze jours, j’ai continué à perfectionner le projet et parfois même je devais debugger le code de mes autres collaborateurs…
                            Ce fût très enrichissant, car en plus de développer de mon côté en me formant sur plusieurs sites, je pouvais poser des questions (presque en illimité) en M. Basquin quand je ne comprenais pas ou bien qu’il fallût m’expliquer pendant une dizaine de minutes comment je devais m’y prendre pour développer ou pour résoudre un problème,
                            au final je m’en sors plutôt bien, avec un quota d’environ un ticket Mantis réalisé chaque jour ou sous 48h.

                        </p>
                        <p>
                            M. Lorre m’a appris faire un petit moteur de recherche à l’aide des mots clés (expressions régulières ou avec des requêtes SQL selon ce qu’il faut rechercher…).
                            Cela m’aidera beaucoup pour la mission que j’ai proposé à mon chef de projet. Actuellement, la hotline de notre société est saturée en raison des nombreux appels des clients ayant des questions sur le logiciel. Ces questions sont très souvent les mêmes et très simples à répondre.
                        </p>
                        <p>
                            L’idée serait ici, de faire une foire aux questions qui sera intégré au logiciel afin de diminuer le nombre d’appels et ainsi faire gagner du temps aux clients.
                            J’ai donc questionné les commerciaux, mon chef de projet ainsi que les autres développeurs pendant toute une après-midi sur les questions les plus fréquentes des clients.
                            Ainsi, durant les jours qui suivent, j’ai pu alors designer et fournir ma maquette de cette future fonctionnalité sur Photoshop, mon chef de projet et M. Levy ont adorés !
                            J’ai donc commencé à coder cette FAQ et j’ai pu donc implémenter un petit moteur de recherche pour chercher une question. La difficulté ici était de rendre plus rapide la recherche.
                            En effet, chaque question était stockée en base de données. Au final, le temps de latence ne venait pas de mon script mais du serveur contenant la base de données qui était trop lent (Microsoft Server 2008 obsolète + configuration matériel insuffisante).
                            Pour finir, j'avais pour mission de travailler sur le calendrier des agents immobiliers. Chaque agent à dans son compte un espace "agenda" (appelé Davical). Ce dernier n'est pas intégré dans E2, c'est un site externe.
                            Je devais développer une fonction permettant lors de l'archivage ou la modification d'un utilisateur, synchroniser avec Davical pour apporter les changements. La difficulté ici était de comprendre les tables et les entrées déja existantes dans la base de données Postegree SQL.
                            Pour information, aucune documentation n'est fournise sur internet, et quasiment personne de la société ne se rappelait comment Davical était codé...
                        </p>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <b><i><h3 style="text-align: center;">Interface de Postegree SQL - PhpPgAdmin</h3></i></b>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-1">
                    <img src="/css/postgres.png" class="e3" alt="Postegree SQL">
                </div>
            </div>
        </div>
    </section>

    <b><i><h3 style="text-align: center;">L'agenda Davical des agents immobiliers</h3></i></b>
    <hr>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-1">
                    <img src="/css/davical.png" class="e3" alt="Davical">
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row contact">
                <div class="col-md-6 text-right">
                    <div class="contacts-data">
                        <h3 >A propos de moi</h3>
                        <a href="#" class="btn btn-default transparent">Mon CV <i class="fa fa-arrow-right button-icon"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contacts-data">
                        <i class="fa fa-paper-plane fa-2x"></i>
                        <span class="contact-text">cyril.vella@yahoo.com</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-phone fa-2x"></i>
                        <span class="contact-text">06.52.41.51.45</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-skype fa-2x"></i>
                        <span class="contact-text">cyril.vella</span>
                    </div>
                </div>
            </div>
            <p class="text-center">
                TOUS DROITS RESERVES. 2017
            </p>
        </div>
    </footer>
