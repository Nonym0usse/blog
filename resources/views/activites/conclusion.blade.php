@extends('app')
@section('content')
    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i> MonBlog</a>
            </div>
            <div class="navbar-collapse collapse" id="bs-navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="{{ route('/') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('entreprise') }}">L'entreprise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('activites') }}">Mes activités</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll"  href="#aboutModal">CYRIL VELLA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="banner">
        <div class="banner-text">
            <b><h1 style="background-color: #000; opacity: 0.5;">BLOG DE STAGE - CYRIL VELLA</h1></b>
        </div>
    </div>
    <h1 class="cursive" style="text-align: center; padding-top: 4%;">Conclusion</h1>
    <hr>
    <section>
    <div class="container">
        <div class="row">
            <article><p>
                    Ce stage chez Immo-One ont été très formateur sur beaucoup de points.
                    Premièrement, je me suis surpassé en termes de compétences techniques, je maîtrise dorénavant de mieux en mieux la programmation orienté objet (grâce à PHP),
                    chose qui m’étais difficile étant donné que j’étais très habitué à la programmation en procédurale.
                </p>
                <p>
                    J’ai beaucoup aimé travailler en entreprise car c’est une approche différente par rapport au travail demandé en cours. En effet, le travail se doit être fonctionnel, simple à comprendre pour l’utilisateur mais aussi il doit être codé de façon de compréhensible pour une maintenance ou une évolution de façon simple. Ce qui m’amène au deuxième point : la rigueur dans le travail, chose que je manipule avec plus de souplesse. Troisième point, le travail d’équipe : on ne peut pas avancer si chacun accompli des tâches sans prévenir les autres développeurs, ainsi cela évite les incompréhensions et le travail hors-sujet ou répétitif. C’est très important de travailler cette compétence pour avancer dans le travail d’entreprise ou à l’école.
                </p>
                <p>
                    Le mot de la fin : Bonne ambiance, travail sérieux / rigoureux et professionnalisme caractérisent cette société, ce fût particulièrement plaisant de travailler avec vous.
                </p>
                <h1 class="cursive" style="text-align: center; padding-top: 4%;">Merci</h1>
            </article>
        </div>
    </div>
        <p style="text-align: center; padding-top: 3%;">PS : J'allais oublié... Je suis pris en alternance pour ma troisième année chez Immo-One.</p>
    </section>

    <footer>
        <div class="container">
            <div class="row contact">
                <div class="col-md-6 text-right">
                    <div class="contacts-data">
                        <h3 >A propos de moi</h3>
                        <a href="#" class="btn btn-default transparent">Mon CV <i class="fa fa-arrow-right button-icon"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contacts-data">
                        <i class="fa fa-paper-plane fa-2x"></i>
                        <span class="contact-text">cyril.vella@yahoo.com</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-phone fa-2x"></i>
                        <span class="contact-text">06.52.41.51.45</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-skype fa-2x"></i>
                        <span class="contact-text">cyril.vella</span>
                    </div>
                </div>
            </div>
            <p class="text-center">
                TOUS DROITS RESERVES. 2017
            </p>
        </div>
    </footer>