@extends('app')
@section('content')
    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i> MonBlog</a>
            </div>
            <div class="navbar-collapse collapse" id="bs-navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="{{ route('/') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('entreprise') }}">L'entreprise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('activites') }}">Mes activités</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll"  href="#aboutModal">CYRIL VELLA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header id="deux" style="background-image: url('css/code.jpg');">
        <div class="header-content">
            <div class="inner">
                <h1 class="cursive">Mes activités</h1>
                <h4>Présentation de mes tâches</h4>
                <hr>
                <a href="#video-background" id="toggleVideo" data-toggle="collapse" class="btn btn-primary btn-xl">Découvrir</a>
            </div>
        </div>
    </header>

    <article>
        <h1 class="cursive">Introduction</h1>
        <hr>
        <div class="container" style="padding-top: 2%;">
            <div class="row">
                Toutes mes activités sont sur le développement du logiciel E2. En effet, ce logiciel développé en PHP 7.0 avec le framework Laravel et Bootstrap, rendant le site responsive pour tous les appareils.
                Il n’est pas encore terminé et ma mission fut de développer des fonctionnalités supplémentaires à partir de l’ancien site Etransac ou en apportant de nouvelles solutions innovantes. Mon travail consiste à faire de l’intégration de fonctionnalités sur le site.
            </div>
        </div>
    </article>

    <section class="publicaciones-blog-home">
        <h1 class="cursive">Mes activités</h1>
        <hr>
        <div class="container" style="padding-top: 1%;">
            <div class="">
                <div class="row-page row">
                    <div class="col-page col-sm-8 col-md-6">
                        <a href="{{ route('activites.1') }}" class="black fondo-publicacion-home">
                            <div class="img-publicacion-principal-home">
                                <img class="" src="https://placeholdit.imgix.net/~text?txtsize=34&txt=&w=500&h=300">
                            </div>
                            <div class="contenido-publicacion-principal-home">
                                <h3>Premières semaines</h3>
                                <p>Cela fait maintenant deux semaines que je travaille chez Immo-one en tant que développeur <span>...</span></p>
                            </div>
                            <div class="mascara-enlace-blog-home">
                                <span>Voir </span>
                            </div>
                        </a>
                    </div>
                    <div class="col-page col-sm-4 col-md-3">
                        <a href="{{ route('activites.2') }}"  class="fondo-publicacion-home">
                            <div class="img-publicacion-home">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?txtsize=34&txt=&w=500&h=300">
                            </div>
                            <div class="contenido-publicacion-home">
                                <h3>Un pas de plus dans l'entreprise</h3>
                                <p>Cela fait maintenant plus de deux semaines que je suis un membre de l’équipage. J’en sais maintenant <span>...</span></p>
                            </div>
                            <div class="mascara-enlace-blog-home">
                                <span>Voir </span>
                            </div>
                        </a>
                    </div>
                    <div class="col-page col-sm-4 col-md-3">
                        <a href="{{ route('activites.3') }}" class="fondo-publicacion-home">
                            <div class="img-publicacion-home">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?txtsize=34&txt=&w=500&h=300">
                            </div>
                            <div class="contenido-publicacion-home">
                                <h3>Je m'en sors plûtot bien !</h3>
                                <p>Cela fait 1 mois que j’ai intégré l’équipe, mais malgré cela j’ai parfois un peu de mal à comprendre le <span>...</span></p>
                            </div>
                            <div class="mascara-enlace-blog-home">
                                <span>Voir </span>
                            </div>
                        </a>
                    </div>
                    <div class="col-page col-sm-4 col-md-3">
                        <a href="{{ route('activites.4') }}" class="fondo-publicacion-home">
                            <div class="img-publicacion-home">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?txtsize=34&txt=&w=500&h=300">
                            </div>
                            <div class="contenido-publicacion-home">
                                <h3>Des tâches de plus en plus complexes</h3>
                                <p>Ces deux semaines ont été consacrés à la même est unique tâche. Il fallait faire un formulaire contenant plus d’une quarantaine de champs<span>...</span></p>
                            </div>
                            <div class="mascara-enlace-blog-home">
                                <span>Voir </span>
                            </div>
                        </a>
                    </div>
                    <div class="hidden-sm col-page col-sm-4 col-md-3">
                        <a href="{{ route('activites.5') }}" class="fondo-publicacion-home">
                            <div class="img-publicacion-home">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?txtsize=34&txt=&w=500&h=300">
                            </div>
                            <div class="contenido-publicacion-home">
                                <h3>De plus en plus de facilités</h3>
                                <p>Pendant ces deux semaines, j’ai passé les trois premiers jours à faire du nettoyage dans la société avec<span>...</span></p>
                            </div>
                            <div class="mascara-enlace-blog-home">
                                <span>Voir </span>
                            </div>
                        </a>
                    </div>
                    <div class="hidden-sm col-page col-sm-4 col-md-3">
                        <a href="{{ route('activites.conclusion') }}" class="fondo-publicacion-home">
                            <div class="img-publicacion-home">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?txtsize=34&txt=&w=500&h=300">
                            </div>
                            <div class="contenido-publicacion-home">
                                <h3>En route vers l'alternance</h3>
                                <p>Ces trois mois passés chez Immo-One ont été très formateur sur beaucoup de points. Premièrement, je me suis surpassé en termes de<span>...</span></p>
                            </div>
                            <div class="mascara-enlace-blog-home">
                                <span>Voir </span>
                            </div>
                        </a>
                    </div>
                    <div class="col-page col-sm-4 col-md-3">
                        <a href="#" class="todas-las-publicaciones-home">
                            <span>Mes articles de fin mai à mi-août 2017</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
