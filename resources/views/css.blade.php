<style>

    .wrapper {
        position: absolute;
        width: 400px;
        height: 400px;
        margin: auto;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        display: flex;
        flex-direction: row;
    }

    .container {
        flex: 1;
        padding: 0 20px;
    }

    div.table-title {
        display: block;
        margin: auto;
        max-width: 600px;
        padding:5px;
        width: 100%;
    }

    .table-title h3 {
        color: #fafafa;
        font-size: 30px;
        font-weight: 400;
        font-style:normal;
        font-family: "Roboto", helvetica, arial, sans-serif;
        text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
        text-transform:uppercase;
    }


    /*** Table Styles **/

    .table-fill {
        background: white;
        border-radius:3px;
        border-collapse: collapse;
        height: 320px;
        margin: auto;
        max-width: 600px;
        padding:5px;
        width: 100%;
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.1);
        animation: float 5s infinite;
    }

    th {
        color:#D5DDE5;;
        background:#1b1e24;
        border-bottom:4px solid #9ea7af;
        border-right: 1px solid #343a45;
        font-size:23px;
        font-weight: 100;
        padding:24px;
        text-align:left;
        text-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        vertical-align:middle;
    }

    th:first-child {
        border-top-left-radius:3px;
    }

    th:last-child {
        border-top-right-radius:3px;
        border-right:none;
    }

    tr {
        border-top: 1px solid #C1C3D1;
        border-bottom-: 1px solid #C1C3D1;
        color:#666B85;
        font-size:16px;
        font-weight:normal;
        text-shadow: 0 1px 1px rgba(256, 256, 256, 0.1);
    }

    tr:hover td {
        background:#4E5066;
        color:#FFFFFF;
        border-top: 1px solid #22262e;
        border-bottom: 1px solid #22262e;
    }

    tr:first-child {
        border-top:none;
    }

    tr:last-child {
        border-bottom:none;
    }

    tr:nth-child(odd) td {
        background:#EBEBEB;
    }

    tr:nth-child(odd):hover td {
        background:#4E5066;
    }

    tr:last-child td:first-child {
        border-bottom-left-radius:3px;
    }

    tr:last-child td:last-child {
        border-bottom-right-radius:3px;
    }

    td {
        background:#FFFFFF;
        padding:20px;
        text-align:left;
        vertical-align:middle;
        font-weight:300;
        font-size:18px;
        text-shadow: -1px -1px 1px rgba(0, 0, 0, 0.1);
        border-right: 1px solid #C1C3D1;
    }

    td:last-child {
        border-right: 0px;
    }

    th.text-left {
        text-align: left;
    }

    th.text-center {
        text-align: center;
    }

    th.text-right {
        text-align: right;
    }

    td.text-left {
        text-align: left;
    }

    td.text-center {
        text-align: center;
    }

    td.text-right {
        text-align: right;
    }


    main {
        width: 500px;
        margin: 10px auto;
        padding: 10px 20px 30px;
        box-shadow: 0 3px 5px rgba(0,0,0,0.2);
    }
    #pie-chart {
        width: 500px;
        height: 250px;
        position: relative;
    }
    /* chart key style */
    #pie-chart::before {
        content: "";
        position: absolute;
        display: block;
        width: 180px;
        height: 95px;
        left: 315px;
        top: 0;
        box-shadow: 1px 1px 0 0 #DDD;
    }
    /* shadow under chart */
    #pie-chart::after {
        content: "";
        position: absolute;
        display: block;
        top: 260px;
        left: 70px;
        width: 170px;
        height: 2px;
        background: rgba(0,0,0,0.1);
        border-radius: 50%;
        box-shadow: 0 0 3px 4px rgba(0,0,0,0.1);
    }

    /** TIMELINE **/


    .timeline {
        list-style: none;
        padding: 20px 0 20px;
        position: relative;
    }

    .timeline:before {
        top: 0;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 3px;
        background-color: #eeeeee;
        left: 50%;
        margin-left: -1.5px;
    }

    .timeline > li {
        margin-bottom: 20px;
        position: relative;
    }

    .timeline > li:before,
    .timeline > li:after {
        content: " ";
        display: table;
    }

    .timeline > li:after {
        clear: both;
    }

    .timeline > li:before,
    .timeline > li:after {
        content: " ";
        display: table;
    }

    .timeline > li:after {
        clear: both;
    }

    .timeline > li > .timeline-panel {
        width: 46%;
        float: left;
        border: 1px solid #d4d4d4;
        border-radius: 2px;
        padding: 20px;
        position: relative;
        -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
        box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline > li > .timeline-panel:before {
        position: absolute;
        top: 26px;
        right: -15px;
        display: inline-block;
        border-top: 15px solid transparent;
        border-left: 15px solid #ccc;
        border-right: 0 solid #ccc;
        border-bottom: 15px solid transparent;
        content: " ";
    }

    .timeline > li > .timeline-panel:after {
        position: absolute;
        top: 27px;
        right: -14px;
        display: inline-block;
        border-top: 14px solid transparent;
        border-left: 14px solid #fff;
        border-right: 0 solid #fff;
        border-bottom: 14px solid transparent;
        content: " ";
    }

    .timeline > li > .timeline-badge {
        color: #fff;
        width: 50px;
        height: 50px;
        line-height: 50px;
        font-size: 1.4em;
        text-align: center;
        position: absolute;
        top: 16px;
        left: 50%;
        margin-left: -25px;
        background-color: #999999;
        z-index: 100;
        border: 2px solid #FFF;
        background: #000;
        border-top-right-radius: 50%;
        border-top-left-radius: 50%;
        border-bottom-right-radius: 50%;
        border-bottom-left-radius: 50%;
    }

    .timeline > li.timeline-inverted > .timeline-panel {
        float: right;
    }

    .timeline > li.timeline-inverted > .timeline-panel:before {
        border-left-width: 0;
        border-right-width: 15px;
        left: -15px;
        right: auto;
    }

    .timeline > li.timeline-inverted > .timeline-panel:after {
        border-left-width: 0;
        border-right-width: 14px;
        left: -14px;
        right: auto;
    }

    .timeline-badge.primary {
        background-color: #2e6da4 !important;
    }

    .timeline-badge.success {
        background-color: #3f903f !important;
    }

    .timeline-badge.warning {
        background-color: #f0ad4e !important;
    }

    .timeline-badge.danger {
        background-color: #d9534f !important;
    }

    .timeline-badge.info {
        background-color: #5bc0de !important;
    }

    .timeline-title {
        margin-top: 0;
        color: inherit;
    }

    .timeline-body > p,
    .timeline-body > ul {
        margin-bottom: 0;
    }

    .timeline-body > p + p {
        margin-top: 5px;
    }

    @media (max-width: 767px) {
        ul.timeline:before {
            left: 40px;
        }

        ul.timeline > li > .timeline-panel {
            width: calc(100% - 90px);
            width: -moz-calc(100% - 90px);
            width: -webkit-calc(100% - 90px);
        }

        ul.timeline > li > .timeline-badge {
            left: 15px;
            margin-left: 0;
            top: 16px;
        }

        ul.timeline > li > .timeline-panel {
            float: right;
        }

        ul.timeline > li > .timeline-panel:before {
            border-left-width: 0;
            border-right-width: 15px;
            left: -15px;
            right: auto;
        }

        ul.timeline > li > .timeline-panel:after {
            border-left-width: 0;
            border-right-width: 14px;
            left: -14px;
            right: auto;
        }
    }

    footer {
        background: url("http://i1378.photobucket.com/albums/ah89/andreykokhanevich/portfolio%20page/footer-bg_zps1imhqalh.jpg");
        -webkit-background-size: cover;
        background-size: cover;
        color: #fff;
    }
    footer h2 {
        margin-top: 7%;
    }
    .contact {
        margin: 5% 0;
    }
    .contacts-data {
        margin: 3% 7%;
    }
    .contacts-data .btn {
        background: transparent;
        padding: 15px 20px;
        color: #fff;
        text-transform: uppercase;
        margin-top: 7%;
    }
    .contacts-data .btn:hover {
        background: #ffb424;
        color: #fff;
    }
    .contact .button-icon {
        padding-left: 10px;
    }
    .contact-text {
        font-family: "Lato", serif;
        font-size: 1.5rem;
        text-transform: uppercase;
        letter-spacing: 0.33rem;
        padding-left: 7%;
    }
    /*footer end*/

    /* media queries */
    @media screen and (min-width: 991px) {
        header .banner {
            background-position: center;
        }
    }
    @media screen and (max-width: 990px) {
        header .banner-content {
            text-align: center;
        }
        .scroll-down {
            display: none;
        }
        .contacts-data {
            text-align: center;
        }
    }
    @media screen and (max-width: 767px) {
        header .banner-content h1 {
            font-size: 4rem;
        }
        header .banner-content h3 {
            font-size: 2.5rem;
        }
        header .social-list {
            display: none;
        }
        .block {
            width: 65%;
            margin: 20px auto;
        }
        .skills img {
            width: 65%;
        }
    }
    @media screen and (max-width: 491px) {
        header .banner-content h1 {
            font-size: 2.5rem;
        }
        header .banner-content h3 {
            font-size: 1.5rem;
        }
    }

    .organnigramme{
        width: 70%;
    }
    .organnigramme-societe
    {
        width: 75%;
    }



    @keyframes bake-pie {
        from {
            transform: rotate(0deg) translate3d(0,0,0);
        }
    }



    .pieID {
        display: inline-block;
        vertical-align: top;
    }
    .pie {
        height: 200px;
        width: 200px;
        position: relative;
        margin: 0 30px 30px 0;
    }
    .pie::before {
        content: "";
        display: block;
        position: absolute;
        z-index: 1;
        width: 100px;
        height: 100px;
        background: #EEE;
        border-radius: 50%;
        top: 50px;
        left: 50px;
    }
    .pie::after {
        content: "";
        display: block;
        width: 120px;
        height: 2px;
        background: rgba(0,0,0,0.1);
        border-radius: 50%;
        box-shadow: 0 0 3px 4px rgba(0,0,0,0.1);
        margin: 220px auto;

    }
    .slice {
        position: absolute;
        width: 200px;
        height: 200px;
        clip: rect(0px, 200px, 200px, 100px);
        animation: bake-pie 1s;
    }
    .slice span {
        display: block;
        position: absolute;
        top: 0;
        left: 0;
        background-color: black;
        width: 200px;
        height: 200px;
        border-radius: 50%;
        clip: rect(0px, 200px, 200px, 100px);
    }
    .legend {
        list-style-type: none;
        padding: 0;
        margin: 0;
        background: #FFF;
        padding: 15px;
        font-size: 13px;
        box-shadow: 1px 1px 0 #DDD,
        2px 2px 0 #BBB;
    }
    .legend li {
        width: 110px;
        height: 1.25em;
        margin-bottom: 0.7em;
        padding-left: 0.5em;
        border-left: 1.25em solid black;
    }
    .legend em {
        font-style: normal;
    }
    .legend span {
        float: right;
    }
.e2
{
    width: 80%;
}

    .e3
    {
        width: 95%;
    }

    .single-blog-item {
        border: 1px solid #dfdede;
        box-shadow: 2px 5px 10px #dfdede;
        margin: 15px auto;
        padding: 5px;
        position: relative;
    }
    .blog-content {
        padding: 15px;
    }
    .blog-content h4 {
        font-size: 16px;
        font-weight: 500;
        margin-bottom: 10px;
        text-transform: uppercase;
    }
    .blog-content h4 a{
        color:#777;
    }
    .blog-content p{
        color: #999;
        font-size: 14px;
        font-weight: 300;
        line-height: 1.3333;
    }
    .blog-date{
    }
    .blog-date {
        position: absolute;
        background: #337ab7;
        top: 35px;
        left: 5px;
        color: #fff;
        border-radius: 0 25px 25px 0;
        padding: 5px 15px;
        font-weight: 700;
    }
    .more-btn {
        background: #337ab7;
        border-radius: 2px;
        display: block;
        height: 30px;
        line-height: 30px;
        margin: 30px auto auto auto;
        text-align: center;
        width: 110px;
        color: #f1f1f1;
    }

    .publicaciones-blog-home {
        padding-bottom: 50px;
        background: url("") no-repeat fixed center center;
        background-size: 100% auto;
    }
    .publicaciones-blog-home h2 {
        text-align: center;
        font-weight: 300;
        margin-bottom: 30px;
        font-size: 44px;
        margin-top: 70px;
    }
    .publicaciones-blog-home h2 b {
        color: #2BBCDE;
    }
    .publicaciones-blog-home .fondo-publicacion-home {
        background: #ffffff;
        border-radius: 3px;
        overflow: hidden;
        height: 400px;
        margin-bottom: 20px;
        display: block;
        color: inherit;
        text-decoration: none;
        position: relative;
    }
    .publicaciones-blog-home .fondo-publicacion-home:hover h3 {
        color: #2BBCDE;
        /*    box-shadow: 0px 4px 3px 3px rgba(0, 0, 0, 0.08);*/
    }
    .publicaciones-blog-home .fondo-publicacion-home:hover .mascara-enlace-blog-home {
        height: 400px;
        width: 100%;
        color: #aaa;
        background-color: #2BBCDE;
        position: absolute;
        top: 0;
        opacity: 0.95;
        -webkit-transition: all 0.4s ease-out 0s;
        -o-transition: all 0.4s ease-out 0s;
        transition: all 0.4s ease-out 0s;
    }
    .publicaciones-blog-home .black {
        background: #2BBCDE;
    }
    .publicaciones-blog-home .fondo-publicacion-home .img-publicacion-principal-home {
        display: inline-block;
        width: 50%;
        overflow: hidden;
        height: 100%;
    }
    .publicaciones-blog-home .fondo-publicacion-home .img-publicacion-principal-home img {
        height: 100%;
        width: auto;
    }
    .publicaciones-blog-home .fondo-publicacion-home .contenido-publicacion-principal-home {
        display: inline-block;
        vertical-align: top;
        width: 49%;
        padding: 0 10px;
    }
    .publicaciones-blog-home .fondo-publicacion-home .contenido-publicacion-principal-home h3 {
        font-weight: 900;
        color: #fff;
        text-transform: uppercase;
        font-size: 30px;
    }
    .publicaciones-blog-home .fondo-publicacion-home .contenido-publicacion-principal-home p {
        color: #ffffff;
        font-size: 16px;
        font-weight: 300;
    }
    .publicaciones-blog-home .fondo-publicacion-home .contenido-publicacion-home {
        padding: 0 10px;
    }
    .publicaciones-blog-home .fondo-publicacion-home .contenido-publicacion-home h3 {
        font-weight: 900;
        font-size: 20px;
        text-transform: uppercase;
    }
    .publicaciones-blog-home .fondo-publicacion-home .img-publicacion-home  {
        overflow: hidden;
        max-height: 180px;
    }
    .mascara-enlace-blog-home {
        height: 400px;
        width: 0%;
        color: #aaa;
        background-color: #2BBCDE;
        position: absolute;
        top: 0;
        opacity: 0.0;
        -webkit-transition: all 0.4s ease-out 0s;
        -o-transition: all 0.4s ease-out 0s;
        transition: all 0.4s ease-out 0s;
        text-align: center;
        padding-top: 180px;
    }
    .mascara-enlace-blog-home span {
        text-align: center;
        max-height: 400px;
        border: 1px solid #fff;
        display: inline-block;
        padding: 10px 30px;
        border-radius: 3px;
        color: #fff;
        font-weight: 900;
        font-size: 16px;
    }
    .publicaciones-blog-home .todas-las-publicaciones-home {
        background: #2BBCDE;
        height: 400px;
        width: 100%;
        display: inline-block;
        padding: 20px;
        text-decoration: none;
        border-radius: 3px;
    }
    .publicaciones-blog-home .todas-las-publicaciones-home span {
        color: #fff;
        font-weight: 900;
        text-transform: uppercase;
        font-size: 25px;
        line-height: 26px;
    }
    @media (max-width: 768px) {
        .publicaciones-blog-home h2 {
            text-align: center;
            font-weight: 300;
            margin-bottom: 30px;
            font-size: 34px;
            margin-top: 70px;
        }
        .publicaciones-blog-home .fondo-publicacion-home {
            background: #ffffff;
            border-radius: 3px;
            overflow: hidden;
            height: inherit;
            margin-bottom: 20px;
            display: block;
            color: inherit;
            text-decoration: none;
            position: relative;
        }
        .publicaciones-blog-home .fondo-publicacion-home .img-publicacion-principal-home {
            display: inline-block;
            width: 100%;
            overflow: hidden;
            height: auto;
        }
        .publicaciones-blog-home .fondo-publicacion-home .img-publicacion-principal-home img {
            height: auto;
            width: 100%;
        }
        .publicaciones-blog-home .black {
            background: #fff;
        }
        .publicaciones-blog-home .fondo-publicacion-home .contenido-publicacion-principal-home {
            display: inline-block;
            vertical-align: top;
            width: 100%;
            padding: 0 10px;
        }
        .publicaciones-blog-home .fondo-publicacion-home .contenido-publicacion-principal-home h3 {
            font-weight: 900;
            color: #333;
            text-transform: uppercase;
            font-size: 20px;
        }
        .publicaciones-blog-home .fondo-publicacion-home .contenido-publicacion-principal-home p {
            color: #333;
            font-size: 14px;
            font-weight: 400;
        }
        .publicaciones-blog-home .todas-las-publicaciones-home {
            background: #2BBCDE;
            height: 100%;
            width: 100%;
            display: inline-block;
            padding: 20px;
            text-decoration: none;
            border-radius: 3px;
        }
    }

    .banner {
        color: white;
        height: 300px;
        display: flex;
        justify-content: center;
        align-items: center;
    }



    .blogShort{ border-bottom:1px solid #ddd;}
    .add{background: #333; padding: 10%; height: 300px;}

    .nav-sidebar {
        width: 100%;
        padding: 8px 0;
        border-right: 1px solid #ddd;
    }
    .nav-sidebar a {
        color: #333;
        -webkit-transition: all 0.08s linear;
        -moz-transition: all 0.08s linear;
        -o-transition: all 0.08s linear;
        transition: all 0.08s linear;
    }
    .nav-sidebar .active a {
        cursor: default;
        background-color: #34ca78;
        color: #fff;
    }
    .nav-sidebar .active a:hover {
        background-color: #37D980;
    }
    .nav-sidebar .text-overflow a,
    .nav-sidebar .text-overflow .media-body {
        white-space: nowrap;
        overflow: hidden;
        -o-text-overflow: ellipsis;
        text-overflow: ellipsis;
    }

    .btn-blog {
        color: #ffffff;
        background-color: #37d980;
        border-color: #37d980;
        border-radius:0;
        margin-bottom:10px
    }
    .btn-blog:hover,
    .btn-blog:focus,
    .btn-blog:active,
    .btn-blog.active,
    .open .dropdown-toggle.btn-blog {
        color: white;
        background-color:#34ca78;
        border-color: #34ca78;
    }
    article h2{color:#333333;}
    h2{color:#34ca78;}
    .margin10{margin-bottom:10px; margin-right:10px;}

</style>