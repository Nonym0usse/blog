@extends('app')

@section('content')
    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i> MonBlog</a>
            </div>
            <div class="navbar-collapse collapse" id="bs-navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="{{ route('/') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('entreprise') }}">L'entreprise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('activites') }}">Mes activités</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll"  href="#aboutModal">CYRIL VELLA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <header style="background-image: url('css/IMG_1452.JPG');">
        <div class="header-content">
            <div class="inner">
                <h1 class="cursive">Bienvenue sur mon blog</h1>
                <h4>Stage été - 2017 - La Duranne</h4>
                <hr>
                <a href="{{ route('activites') }}" id="voir" class="btn btn-primary btn-xl">Découvrir</a>
            </div>
        </div>

    </header>
    <section id="one">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 text-center">
                    <h2 class="cursive" style="text-align: center; color: #FFF !important;">A propos de ce blog</h2>
                    <hr>
                    <br>
                    <p>
                        Vous voici sur mon blog. Je partagerai avec vous mon expérience très enrichissante en entreprise.
                        J’ai commencé mon stage le 29 mai 2017, dans une entreprise située à la Duranne (Aix-En-Provence) pour une durée de 3 mois dans le but de valider ma deuxième année d’études en informatique.
                        Vous trouverez sur ce site, la présentation de l’entreprise (ces activités, le type de clientèle, les projets futurs de l’entreprise…), mes missions auxquelles j’ai participé et le bilan sur cette période en entreprise.
                        J'ai codé ce site entièrement avec Laravel, j'espère qu'il vous plaîra.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="three" class="no-padding">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-6">
                    <a class="gallery-box" href="{{ route('entreprise') }}">
                        <img src="css/IMG_1473b.jpg" class="img-responsive" alt="Image 1">
                        <div class="gallery-box-caption">
                            <div class="gallery-box-content">
                                <div>
                                    <i class="icon-lg ion-ios-search"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="gallery-box" href="{{ route('entreprise') }}">
                        <img src="css/img_1472a.jpg" class="img-responsive" alt="Image 2">
                        <div class="gallery-box-caption">
                            <div class="gallery-box-content">
                                <div>
                                    <i class="icon-lg ion-ios-search"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="gallery-box" href="{{ route('entreprise') }}">
                        <img src="css/IMG_1452.jpg" class="img-responsive" alt="Image 3">
                        <div class="gallery-box-caption">
                            <div class="gallery-box-content">
                                <div>
                                    <i class="icon-lg ion-ios-search"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4" style="padding-bottom: 100px;">
      <h2 class="cursive" style="text-align: center; color: #FFF !important;">Où se situe l'entreprise ?</h2>
      <hr>
    </div>

    <section class="container">
        <div class="row">
            <div id="map"></div>
              <script>
              var marker;

                function initMap() {
                       var aix = {lat: 43.492861, lng: 5.346242999999959};
                       var map = new google.maps.Map(document.getElementById('map'), {
                         zoom: 10,
                         center: aix
                       });
               var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'+
                '<div id="bodyContent">'+
                '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large ' +
                'sandstone rock formation in the southern part of the '+
                'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '+
                'south west of the nearest large town, Alice Springs; 450&#160;km '+
                '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '+
                'features of the Uluru - Kata Tjuta National Park. Uluru is '+
                'sacred to the Pitjantjatjara and Yankunytjatjara, the '+
                'Aboriginal people of the area. It has many springs, waterholes, '+
                'rock caves and ancient paintings. Uluru is listed as a World '+
                'Heritage Site.</p>'+
                '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'+
                'https://en.wikipedia.org/w/index.php?title=Uluru</a> '+
                '(last visited June 22, 2009).</p>'+
                '</div>'+
                '</div>';

                    var infowindow = new google.maps.InfoWindow({
                          content: contentString
                        });

                       var marker = new google.maps.Marker({
                         position: aix,
                         draggable: true,
                         animation: google.maps.Animation.DROP,
                         map: map,
                         title: 'Hello World!'
                       });
                        marker.addListener('click', toggleBounce, function(){
                          infowindow.open(map, marker);
                        });
                  }
                function toggleBounce()
                {
                    if (marker.getAnimation() !== null)
                   {
                      marker.setAnimation(null);
                    } else {
                      marker.setAnimation(google.maps.Animation.BOUNCE);
                    }
                  }
                   </script>
                 <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPxVw695EIkZECp2fnpdhYTewbxCGIlXE&callback=initMap"></script>
            </div>
    </section>
        <h1 class="cursive" style="text-align: center; padding-top: 4%;">Divers</h1>
    <hr>
    <section id="three">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-lg-4 col-sm-6">
                    <a class="gallery-box" href="{{ route('activites.1') }}">
                        <img src="css/copie.png" class="img-responsive" alt="Image 1">
                        <div class="gallery-box-caption">
                            <div class="gallery-box-content">
                                <div>
                                    <i class="icon-lg ion-ios-search"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="gallery-box" href="{{ route('activites.4') }}">
                        <img src="css/copiexml.png" class="img-responsive" alt="Image 2">
                        <div class="gallery-box-caption">
                            <div class="gallery-box-content">
                                <div>
                                    <i class="icon-lg ion-ios-search"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a class="gallery-box" href="{{ route('entreprise') }}">
                        <img src="css/IMG_1572.jpg" class="img-responsive" alt="Image 3">
                        <div class="gallery-box-caption">
                            <div class="gallery-box-content">
                                <div>
                                    <i class="icon-lg ion-ios-search"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                    </a>
                </div>
            </div>
        </div>
        <h2 class="cursive" style="text-align: center; padding-top: 4%; color: #FFF !important;"> En savoir plus - Mes coordonées</h2>
    </section>

    <footer>
            <div class="container">
                <div class="row contact">
                    <div class="col-md-6 text-right">
                        <div class="contacts-data">
                            <h3 >A propos de moi</h3>
                            <a href="#" class="btn btn-default transparent">Mon CV <i class="fa fa-arrow-right button-icon"></i></a>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contacts-data">
                            <i class="fa fa-paper-plane fa-2x"></i>
                            <span class="contact-text">cyril.vella@yahoo.com</span>
                        </div>
                        <div class="contacts-data">
                            <i class="fa fa-phone fa-2x"></i>
                            <span class="contact-text">06.52.41.51.45</span>
                        </div>
                        <div class="contacts-data">
                            <i class="fa fa-skype fa-2x"></i>
                            <span class="contact-text">cyril.vella</span>
                        </div>
                    </div>
                </div>
                <p class="text-center">
                    TOUS DROITS RESERVES. 2017
                </p>
            </div>
        </footer>
@stop

