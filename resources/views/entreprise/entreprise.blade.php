@extends('app')
@section('content')

    <nav id="topNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#first"><i class="ion-ios-analytics-outline"></i> MonBlog</a>
            </div>
            <div class="navbar-collapse collapse" id="bs-navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="page-scroll" href="{{ route('/') }}">Accueil</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('entreprise') }}">L'entreprise</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="{{ route('activites') }}">Mes activités</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll"  href="#aboutModal">CYRIL VELLA</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <header id="deux" style="background-image: url('css/IMG_1472c.JPG');">
        <div class="header-content">
            <div class="inner">
                <h1 class="cursive">L'entreprise</h1>
                <h4>Présentation de l'entreprise</h4>
                <hr>
                <a href="#video-background" id="toggleVideo" data-toggle="collapse" class="btn btn-primary btn-xl">Découvrir</a>
            </div>
        </div>
    </header>

    <section id="one" style="padding: 0 !important;">
        <h1 class="cursive" style="text-align: center; padding-top: 4%;">Présentation de l'entreprise - Aspect Juridique*</h1>
        <div class="container-fluid" style="padding-top: 3%;">
        <table class="table-fill">
            <thead>
            <tr>
                <th class="text-left">Champs</th>
                <th class="text-left">Renseignements juridiques</th>
            </tr>
            </thead>
            <tbody class="table-hover">
            <tr>
                <td class="text-left">Dénomination</td>
                <td class="text-left">ALVEEN</td>
            </tr>
            <tr>
                <td class="text-left">Adresse</td>
                <td class="text-left">510 Rue René Descartes - 13100 Aix-En-Provence</td>
            </tr>
            <tr>
                <td class="text-left">SIREN</td>
                <td class="text-left">353 508 336</td>
            </tr>
            <tr>
                <td class="text-left">Activité (APE)</td>
                <td class="text-left">Edition de logiciels applicatifs (5829C)</td>
            </tr>
            <tr>
                <td class="text-left">Forme juridique</td>
                <td class="text-left">SA à conseil d'administration</td>
            </tr>
            <tr>
                <td class="text-left">Date de création</td>
                <td class="text-left">19-02-1990</td>
            </tr>
            <tr>
                <td class="text-left">Effectif</td>
                <td class="text-left">16 salariés</td>
            </tr>
            <tr>
                <td class="text-left">Chiffre d'affaire</td>
                <td class="text-left">894 132,00 € (2010)</td>
            </tr>
            </tbody>
        </table>
            <center><p style="padding-top: 1%;">* Données issues de la société + site societe.com</p></center>
        </div>
    </section>


    <section id="two" style="padding: 0 !important;">
        <div class="container">
            <div class="page-header">
                <h1 class="cursive" style="text-align: center; padding-top: 4%;">Historique de Immo-one</h1>
            </div>
            <ul class="timeline">
                <li>
                    <div class="timeline-badge"></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Entrée en bourse de Alveen - Fusion avec Immo-one</h4>
                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Septembre 2017</small></p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                M.Levy, le PDG, souhaite fusionner les deux sociétés : Alveen et Immo-one car à l’heure actuelle la société Alveen possède une meilleure notoriété sur la clientèle.
                            </p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-badge"></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Création de E2 - Logiciel web full-responsive</h4>
                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Octobre 2015</small></p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                E2 est un logiciel reprenant les mêmes fonctionnalités de etransac, mais apporte plus de contenu en utilisant les dernières technologies (full responsive, développé avec Bootstrap & Laravel 5.1) et il est beaucoup plus intuitif et rapide que le précédent.
                                Actuellement, la société migre progressivement les clients du logiciel Etransac vers ce logiciel.
                            </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-badge"></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Présentation du réseau social de Alveen sur BFM TV + Création</h4>
                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> Juin 2015 - 2014</small></p>
                        </div>
                        <div class="timeline-body">
                            <p>
                                Le réseau social conçu pour mettre en relation les agents immobiliers et les acquéreurs particuliers, via une plateforme de partage (messagerie, création d’un network de collaborateurs, mise en relation d’un bien, etc…) est développé pendant deux ans puis fut présenté sur BFM Business par M.Levy.
                                Actuellement, le projet est en arrêt de développement, mais une fois que le projet trouvera des investisseurs, le site sera remis à jour très prochainement.
                            </p>
                        </div>
                    </div>
                </li>
                <li class="timeline-inverted">
                    <div class="timeline-badge"></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Création de Etransac</h4>
                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 2006</small></p>
                        </div>
                        <div class="timeline-body">
                        <p>
                            Etransac est un logiciel proposant des services web destinés aux professionnels de l’immobiler.
                            Il permet de faire la gestion de transactions immobilières (biens & acquéreurs).
                            L’agent immobilier remplit une fiche sur le logiciel permettant de créer un bien ou de créer un acquéreur, bien qui une fois complété (photos, description, prix…) sera visible sur un site vitrine tel que Leboncoin, SeLoger, etc…
                            Le design du site à été refait en 2011, avec le logiciel WebDev.
                        </p>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="timeline-badge"></div>
                    <div class="timeline-panel">
                        <div class="timeline-heading">
                            <h4 class="timeline-title">Création de la société Immo-one</h4>
                            <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 1990</small></p>
                        </div>
                        <div class="timeline-body">
                            <p>La société Immo-one à été crée dans les années 1990 par le fondateur M.Levy Mickaël.</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </section>

    <section id="five" style="padding: 0 !important;">
        <h1 class="cursive" style="text-align: center;">Répartition des activités principales</h1>
        <div class="container" style="padding-top: 3%;">
            <div class="row">
                <p>Voici une liste des prestations de la société (voir ci-dessous), la gestion de nos sites de transactions immobilières occupent environ 50% de l'activité de Immo-one,
                    ensuite nous avons un quota de 30% sur la gestion du réseau social Alveen, 10% occupent la création de sites internet sur mesure pour les professionnel,
                    et les 10% restants sont réservés à la vente de matériels (ex: Panono 360°, imprimantes, etc...).
                </p>
                <b><i><p style="text-align: center;">Conception de logiciels de transactions immobilières pour les particuliers (e2 et etransac)</p></i></b>
                <hr>
                <div class="col-sm-offset-2">
                    <img src="css/e2.PNG" class="e2">
                </div>
                <br>
                <b><i><p style="text-align: center;">Création d'un réseau social immobilier</p></i></b>
                <hr>
                <div class="col-sm-offset-2">
                    <img src="css/alveen.jpeg" class="e2">
                </div>
                <br>
                <b><i><p style="text-align: center;">Les médias parlent d'Immo-One ! Passage de M.Levy sur BFM TV</p></i></b>
                <hr>
                <div class="col-sm-offset-2">
                        <video  style="width: 80%;" controls src="css/videobfm.mp4" style="object-fit: fill;" jw-played></video>
                    <br>
                </div>
                <br>
                <b><i><p style="text-align: center;">Vente de panono*</p></i></b>
                <hr>
                <div class="col-sm-offset-2">
                    <img src="css/IMG_1572.JPG" class="e2">
                </div>
                <p style="text-align: center"> *De quoi s'agit-il ?</p>
                <p style="text-align: center;">
                    C'est un appareil photo / caméra vidéo prenant des captures en très haute résolution à 360° (idéal pour les visites en VR).<br>
                    Le prix à payer pour ce bijou s'éléve tout de même à 1800€ HT pour un agent immobilier...
                </p>
            </div>
        </div>
    </section>

    <section id="four" style="padding: 0 !important;">
        <h1 class="cursive" style="text-align: center; padding-top: 4%;">Organigramme de la société</h1>
        <div class="container" style="padding-top: 3%;">
            <div class="row">
                <p>La société compte 19 salariés répartis dans deux pôles : Une équipe commerciale / hotline et un pôle développeur front-end & back-end.
                    L'effectif salarial va très fortement augmenté d'ici les prochains mois en raison de la campagne de publicité du nouveau logiciel E2.
                    Nous avons beaucoup de profils différents dans la société : beaucoup de salariés travaillant ici depuis plus de quinze ans, d'autres sont arrivés dans
                    le courant de l'année et ont tous faits des études supérieurs (parcours ingénieurs informatiques, écoles de commerces, etc...). La moyenne d'âge
                    de Immo-One est d'environ 35 ans.
                </p>
                <div class="col-sm-offset-2">
                    <img src="css/Organigramme-salarie.png" class="organnigramme">
                </div>
            </div>
        </div>
    </section>

    <section id="five" style="padding: 0 !important;">
        <h1 class="cursive" style="text-align: center;">Structure de la société</h1>
        <div class="container" style="padding-top: 3%;">
            <div class="row">
                <p>
                    Technolog est le nom juridique des deux sociétés, Immo-one et Alveen (nom commerciaux).
                    Il est prévu que la société Immo-one fusionne avec Alveen en raison d'une meilleur notoriété de Alveen.
                </p>
                <div class="col-sm-offset-3">
                    <img src="css/organni.png" class="organnigramme-societe">
                </div>
            </div>
        </div>
    </section>
    <br>
    <h1 class="cursive" style="text-align: center;">Plan des locaux de la société</h1>
    <section id="six" style="padding: 0 !important;">
        <div class="container" style="padding-top: 3%;">
            <div class="row">
                <center><p>(Ci-dessous le plan des locaux en open-space, prévu pour la rentrée).</p></center>
                <div class="col-sm-offset-3">
                    <img src="css/IMG_1602.JPG" class="e2">
                </div>
                <br>
                <p style="padding-top: 1%; padding-bottom: 1%;">
                    Actuellement, les locaux comporent des cloisons entre chaques bureaux (pôle développeur et commercial).
                    Un des gros points faibles de cette société, est le manque de communication entre les divers équipes ce qui découle souvent
                    à des incompréhensions lors d'une demande d'un client à l'équipe commercial (exemple : un bug sur le logiciel), information qui parfois ne remonte pas
                    à l'équipe de développeur, provoquant du retard et souvent cela créer du mécontentement de la part du client.
                    <br>
                    Nous avons enfin trouvé la solution : L'idée est de travailler en open-space avec tout le monde en détruisant les cloisons. Ainsi, nous travaillerons
                    beaucoup plus efficacement.
                </p>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row contact">
                <div class="col-md-6 text-right">
                    <div class="contacts-data">
                        <h3 >A propos de moi</h3>
                        <a href="#" class="btn btn-default transparent">Mon CV <i class="fa fa-arrow-right button-icon"></i></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contacts-data">
                        <i class="fa fa-paper-plane fa-2x"></i>
                        <span class="contact-text">cyril.vella@yahoo.com</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-phone fa-2x"></i>
                        <span class="contact-text">06.52.41.51.45</span>
                    </div>
                    <div class="contacts-data">
                        <i class="fa fa-skype fa-2x"></i>
                        <span class="contact-text">cyril.vella</span>
                    </div>
                </div>
            </div>
            <p class="text-center">
                TOUS DROITS RESERVES. 2017 
            </p>
        </div>
    </footer>
@stop