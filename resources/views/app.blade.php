<html>
<script src="{{ asset('js/app.js') }}"></script>
<link rel="stylesheet" href="{{ asset('css/app.css') }}" />
<link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
<link rel="stylesheet" type="text/css" href="/css/font-awesome.css">
<body>
@include('css')
    @yield('content')
    @yield('navbar')
</body>
</html>
